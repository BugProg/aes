
#include <stdio.h>
#include <stdlib.h>

#include "AES.h"

int main() {
    State *state = calloc(1, sizeof(State));
    state->val[0][0] = 0x00;
    state->val[0][1] = 0x11;
    state->val[0][2] = 0x22;
    state->val[0][3] = 0x33;

    state->val[1][0] = 0x00;
    state->val[1][1] = 0x11;
    state->val[1][2] = 0x22;
    state->val[1][3] = 0x33;

    state->val[2][0] = 0x00;
    state->val[2][1] = 0x11;
    state->val[2][2] = 0x22;
    state->val[2][3] = 0x33;

    state->val[3][0] = 0x00;
    state->val[3][1] = 0x11;
    state->val[3][2] = 0x22;
    state->val[3][3] = 0x33;

    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            printf("%b ", state->val[i][j]);
        }
        printf("\n");
    }

    shiftRows(state);
    invShiftRows(state);

    printf("\n\n");
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            printf("%b ", state->val[i][j]);
        }
        printf("\n");
    }

    free(state);

//    AES_128 aes;
//    byte message  [16] = { 0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88, 0x99, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF };
//    byte cipherKey[16] = { 0x00, 0x0E, 0x51, 0xEA, 0x00, 0x0E, 0x51, 0xEA, 0x00, 0x0E, 0x51, 0xEA, 0x00, 0x0E, 0x51, 0xEA };
//    int i;
//
//    setCipherKey(&aes, cipherKey);
//    encrypt128(&aes, message);
//
//    for (i = 0; i < 16; i++)
//        printf("%02x ", message[i]);
//    printf("\n");
//
//    decrypt128(&aes, message);
//
//    for (i = 0; i < 16; i++)
//        printf("%02x ", message[i]);
//    printf("\n");
//
//    return EXIT_SUCCESS;
}